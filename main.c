/*******************************************************************************
 *
 * BootSequence: Test project to look on to boot process for MSP430 MCU
 *
 *    BootSequence:              copyright � 2015, Dmytro Bernyk.
 *
 *    THIS SOFTWARE IS PROVIDED BY THE DMITRY BERNYK AND CONTRIBUTORS "AS IS"
 *    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 *    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DMITRY FRANK OR CONTRIBUTORS BE
 *    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *    THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

#include "io430.h"
#include <stdint.h>
#include <intrinsics.h>

void main(void) {
	// Stop watchdog timer to prevent time out reset
	WDTCTL = WDTPW + WDTHOLD;

	BCSCTL3 = LFXT1S1;
	DCOCTL = 0x00;
	DCOCTL = CALDCO_1MHZ;
	BCSCTL1 = CALBC1_1MHZ;

	P1DIR |= 0x01;
	P1OUT |= 0x00;

	TA0CTL = TASSEL0 | MC0;
	//TAIFG   Timer A counter interrupt flag
	//TAIE    Timer A counter interrupt enable
	//TACLR   Timer A counter clear
	//MC0     Timer A mode control 0
	//MC1     Timer A mode control 1
	//ID0     Timer A clock input divider 0
	//ID1     Timer A clock input divider 1
	//TASSEL0 Timer A clock source select 0
	//TASSEL1 Timer A clock source select 1

	TA0CCR0 = 30000;
	TA0CCTL0 = CCIE;
	//CCIFG          Capture/compare interrupt flag
	//COV            Capture/compare overflow flag
	//OUT            PWM Output signal if output mode 0
	//CCI            Capture input signal (read)
	//CCIE           Capture/compare interrupt enable
	//OUTMOD0        Output mode 0
	//OUTMOD1        Output mode 1
	//OUTMOD2        Output mode 2
	//CAP            Capture mode: 1 /Compare mode : 0
	//SCCI           Latched capture signal (read)
	//SCS            Capture sychronize
	//CCIS0          Capture input select 0
	//CCIS1          Capture input select 1
	//CM0            Capture mode 0
//CM1            Capture mode 1

	__enable_interrupt();

	LPM3;
	while (1);
}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer0_A0(void) {
	static volatile uint8_t state = 0;
	if (state == 1) {
		TA0CCR0 = 60000;
		state = 0;
		P1OUT &= ~0x01;
	} else {
		TACCR0 = 100;
		state = 1;
		P1OUT |= 0x01;
	}
}
